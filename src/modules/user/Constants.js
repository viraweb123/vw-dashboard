export default {
	//------------------------------------------------------------
	// Resources Types
	//------------------------------------------------------------
	AMD_USER_ROLES_RT: '/user/roles',
	AMD_USER_GROUPS_RT: '/user/groups',
	
	AMD_USER_ACCOUNTS_RT: '/user/accounts',
	AMD_USER_ACCOUNT_ID_RT: '/user/account#id',

	//------------------------------------------------------------
	// Storage path
	//------------------------------------------------------------
	AMD_USER_ACCOUNTS_SP: '/user/accounts',
	AMD_USER_GROUPS_SP: '/user/groups',
	AMD_USER_ROLES_SP: '/user/roles',

	//------------------------------------------------------------
	// Actions
	//------------------------------------------------------------
	AMD_USER_ACCOUNT_CREATE_ACTION: 'amd.user.accounts.create',
	AMD_USER_ACCOUNTS_OPENEDITOR_ACTION: 'amd.user.accounts.openEditor',


	//------------------------------------------------------------
	// Wizards
	//------------------------------------------------------------
	AMD_USER_ACCOUNT_CREATE_WIZARD: '/user/wizards/account-create',
}